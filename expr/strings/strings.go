package strings

import "strings"

func NewExprs() map[string]interface{} {
	return map[string]interface{}{
		"ReplaceAll": replaceAll,
		"Contains":   contains,
		"SplitAfter": splitAfter,
	}
}

func replaceAll(s, old, new string) string {
	return strings.ReplaceAll(s, old, new)
}

func contains(s, substr string) bool {
	return strings.Contains(s, substr)
}

func splitAfter(s, sep string) []string {
	return strings.SplitAfter(s, sep)
}
